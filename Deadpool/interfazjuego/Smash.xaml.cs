﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace InterfazJuego
{
    /// <summary>
    /// Lógica de interacción para Smash.xaml
    /// </summary>
    public partial class Smash : Window
    {

        DispatcherTimer t1;
        DispatcherTimer t2;
        int puntuacion = 0;
        int tiempo = 45;
        double intervalo = 1000.0;
        double intervalo2 = 1225.0;
        private MainWindow mainWindow;

        public Smash(MainWindow mainWindow)
        {

            this.mainWindow = mainWindow;

            InitializeComponent();

            t1 = t2 = new DispatcherTimer();
            t1.Interval = TimeSpan.FromMilliseconds(intervalo);
            t2.Interval = TimeSpan.FromMilliseconds(intervalo2);

            t1.Tick += new EventHandler(contador);
            t2.Tick += new EventHandler(mover);

            t1.Start();
            t2.Start();

        }

        private void mover(object sender, EventArgs e)
        {
            lblMasUno.Visibility = Visibility.Hidden;
            Random r = new Random(DateTime.Now.Millisecond);

            Grid.SetRow(cnvSpiderman, r.Next(4));
            Grid.SetColumn(cnvSpiderman, r.Next(4));
        }

        private void contador(object sender, EventArgs e)
        {

            lblTiempo.Content = "Tiempo: " + tiempo;
            if (tiempo == 0)
            {
                t1.Stop();
                t2.Stop();
                grdShooter.IsEnabled = false;
                lblFinal.Content = "¡Tiempo Finalizado!\n\nHas conseguido:\n" + puntuacion + " puntos.\n\nEstos puntos\nrecuperarán\ndiversión.";
                cnvFinal.Visibility = Visibility.Visible;

                mainWindow.anadirPuntos(puntuacion);

            }

            else
            {
                tiempo = tiempo - 1;
            }
        }

        private void sumarPunto(object sender, MouseButtonEventArgs e)
        {
            lblMasUno.Visibility = Visibility.Visible;
            puntuacion += 1;
            lblPuntuacion.Content = "Puntuación: " + puntuacion;

            if (puntuacion == 10) {
                imgTirita.Visibility = Visibility.Visible;
            }

            if (puntuacion == 20) {
                imgTirita2.Visibility = Visibility.Visible;
            }
        }

    }
}
