﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Speech.Recognition;
using System.Diagnostics;

namespace InterfazJuego
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int minutos = 0;
        int segundos = 0;
        double intervalo = 10000.0;
        double segundo = 1000.0;

        Boolean muted = false;
        Boolean reconocimientoVozBool = false;

        DispatcherTimer t1;
        DispatcherTimer t2;

        Avatar avatar = new Avatar(100, 100, 100);

        Storyboard parpadeo, muerteDesp, giro;
        Storyboard enfadado, contento, llorar;
        Storyboard enfermo, cansado, grito, eing;

        Storyboard triste,sorpresa, amor, sospechoso, risis, lengua, nuse, chulo, angel, besi, pensar, piyin, ja;

        Smash smash;

        static SpeechRecognitionEngine recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("ES-ES"));
        
        public MainWindow()
        {
            t1 = new DispatcherTimer();
            t1.Interval = TimeSpan.FromMilliseconds(intervalo);
            t1.Tick += new EventHandler(reloj);
            t1.Start();

            t2 = new DispatcherTimer();
            t2.Interval = TimeSpan.FromMilliseconds(segundo);
            t2.Tick += new EventHandler(tiempoMuerte);
            t2.Start();

            iniciarRecursosReconocimientoVoz();

            this.InitializeComponent();

            parpadeo = (Storyboard)this.Resources["Parpadeo"];
            parpadeo.Begin();

            enfadado = (Storyboard)this.Resources["Enfado"];
            contento = (Storyboard)this.Resources["Contento"];
            llorar = (Storyboard)this.Resources["Llorar"];
            triste = (Storyboard)this.Resources["Triste"];

            sorpresa = (Storyboard)this.Resources["Sorpresa"];
            amor = (Storyboard)this.Resources["Amor"];
            sospechoso = (Storyboard)this.Resources["Sospechoso"];
            risis = (Storyboard)this.Resources["Risis"];
            lengua = (Storyboard)this.Resources["Lengua"];
            nuse = (Storyboard)this.Resources["Nuse"];
            chulo = (Storyboard)this.Resources["Chulo"];
            angel = (Storyboard)this.Resources["Angel"];
            besi = (Storyboard)this.Resources["Besi"];
            pensar = (Storyboard)this.Resources["Pensar"];
            piyin = (Storyboard)this.Resources["Piyin"];
            ja = (Storyboard)this.Resources["Ja"];

        }

        private int Dado(int max)
        {
            Random midado = new Random();
            return 1 + midado.Next(max);
        }

        private void reloj(object sender, EventArgs e)
        {
            int handicapEnergia = Dado(10);
            int handicapHambre = Dado(5);
            int handicapDiversion = Dado(20);

            pbEnergia.Value -= handicapEnergia;
            avatar.Energia -= handicapEnergia;
            pbHambre.Value -= handicapHambre;
            avatar.Apetito -= handicapHambre;
            pbDiversion.Value -= handicapDiversion;
            avatar.Diversion -= handicapDiversion;

            estadosAnimo();

        }

        private void btnDormir_Click(object sender, RoutedEventArgs e)
        {
            Dormir();
        }

        private void btnComer_Click(object sender, RoutedEventArgs e)
        {
            Comer();
        }

        private void btnJugar_Click(object sender, RoutedEventArgs e)
        {
            Jugar();
        }

        private void btnJuego_Click(object sender, RoutedEventArgs e)
        {
            Juego();
        }

        private void estadosAnimo()
        {
            if (pbEnergia.Value <= 50)
            {
                cansado = (Storyboard)this.Resources["Cansado"];
                cansado.Begin();
            }
            if (pbHambre.Value <= 50)
            {
                grito = (Storyboard)this.Resources["Grito"];
                grito.Begin();
            }

            if (pbDiversion.Value <= 50)
            {
                eing = (Storyboard)this.Resources["Eing"];
                eing.Begin();
            }


            if (pbEnergia.Value <= 25 && pbHambre.Value <= 25 && pbDiversion.Value <= 25)
            {
                enfermo = (Storyboard)this.Resources["Enfermo"];
                enfermo.Begin();
            }
        }
        private void Muerte(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            if (pbEnergia.Value <= 0 && pbHambre.Value <= 0 && pbDiversion.Value <= 0) {
                muerteDesp = (Storyboard)this.Resources["Muerte"];
                muerteDesp.Begin();
                giro = (Storyboard)this.Resources["Giro"];
                giro.Begin();

                btnComer.IsEnabled = false;
                btnDormir.IsEnabled = false;
                btnJuego.IsEnabled = false;
                btnJugar.IsEnabled = false;
                btnJuego.IsEnabled = false;
                cnvVoz.IsEnabled = false;
                imgSound.IsEnabled = false;

                recognizer.RecognizeAsyncStop();

                lblMuerte.Content = "Ups... Me he largado.\nEsta vez has conseguido\nmantenerme durante\n" + minutos + ":" + segundos + " minutos." + "\n\n¡Intenta superarlo\nla próxima vez!";
                
            }

        }

        private void tiempoMuerte(object sender, EventArgs e) {

            segundos += 1;

            if (segundos == 60) {
                minutos += 1;
                segundos = 0;
            }

        }

        private void frente(object sender, EventArgs e)
        {
            t1.Start();

        }     

        #region sonido

        private void sonidoFondo_MediaEnded(object sender, System.Windows.RoutedEventArgs e)
        {
            sonidoFondo.Position = TimeSpan.Zero;
            sonidoFondo.LoadedBehavior = MediaState.Play;
        }

        private void imgSound_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (muted == false)
            {
                muted = true;
                sonidoFondo.LoadedBehavior = MediaState.Pause;
                imgSound.Source = new BitmapImage(new Uri(@"img_Sonido.png", UriKind.Relative));
            }
            else
            {
                muted = false;
                sonidoFondo.LoadedBehavior = MediaState.Play;
                imgSound.Source = new BitmapImage(new Uri(@"img_Silencio.png", UriKind.Relative));
            }
        }

        #endregion sonido

        #region voz

        private void iniciarRecursosReconocimientoVoz() {

            
            Choices acciones = new Choices();
            acciones.Add(new string[] {"Duérmete", "Come un taco", "Haz malabares", "Spiderman", "Hay tacos de comida",
                "Se han terminado los tacos","Chimichanga", "Llorar" ,"Te quiero", "No se quien eres", "Todo son risis",
                "Eres muy tonto", "Que has roto", "Que pillín estás hecho", "Has acabado con toda la comida" ,"Lo has hecho perfecto", "Menudo angelito eres", "Dame un beso", "Tres por dos"
            });

            GrammarBuilder gb = new GrammarBuilder();
            gb.Append(acciones);

            Grammar g = new Grammar(gb);

            recognizer.LoadGrammar(g);

            recognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(eventosVoz);
            recognizer.SetInputToDefaultAudioDevice();     
            
        }

        private void eventosVoz(object sender, SpeechRecognizedEventArgs e)
        {
            
            switch (e.Result.Text) {
                case "Duérmete":
                    Dormir();
                    break;
                case "Come un taco":
                    Comer();
                    break;
                case "Haz malabares":
                    Jugar();
                    break;
                case "Spiderman":
                    enfadado.Begin();
                    break;
                case "Hay tacos de comida":
                    contento.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_lalala.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Se han terminado los tacos":
                    triste.Begin();
                    break;
                case "Chimichanga":
                    sorpresa.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Chimichanga.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Llorar":
                    llorar.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Llorar.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Te quiero":
                    amor.Begin();
                    break;
                case "No se quien eres":
                    sospechoso.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Sospechoso.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Todo son risis":
                    risis.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Risa.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Eres muy tonto":
                    lengua.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Mama.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Que has roto":
                    nuse.Begin();
                    break;
                case "Has acabado con toda la comida":
                    ja.Begin();
                    break;
                case "Que pillín estás hecho":
                    piyin.Begin();
                    break;
                case "Lo has hecho perfecto":
                    chulo.Begin();
                    break;
                case "Menudo angelito eres":
                    angel.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Angel.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Dame un beso":
                    besi.Begin();
                    sonidosDeadpool.Source = new Uri(@"snd_Deadpool_Beso.mp3", UriKind.Relative);
                    sonidosDeadpool.LoadedBehavior = MediaState.Play;
                    break;
                case "Tres por dos":
                    pensar.Begin();
                    break;

            }
        }

        private void reconocimientoVoz(object sender, MouseButtonEventArgs e)
        {
            if (reconocimientoVozBool == false) {
                reconocimientoVozBool = true;

                rctBarra.Fill = new SolidColorBrush(Color.FromRgb(50, 150, 5));

                cnvVozOff.Visibility = Visibility.Hidden;
                cnvVozOn.Visibility = Visibility.Visible;

                recognizer.RecognizeAsync(RecognizeMode.Multiple);

            }

            else {
                reconocimientoVozBool = false;

                rctBarra.Fill = new SolidColorBrush(Color.FromRgb(135, 0, 0));
              
                cnvVozOff.Visibility = Visibility.Visible;
                cnvVozOn.Visibility = Visibility.Hidden;

                recognizer.RecognizeAsyncStop();
            }   
        }

        #endregion voz

        #region acciones
        private void Dormir()
        {
            parpadeo.Stop();
            pbEnergia.Value += Dado(60);
            Storyboard dormir = (Storyboard)this.Resources["Dormir"];
            dormir.Begin();
        }

        private void Comer()
        {
            pbHambre.Value += Dado(60);
            Storyboard comer = (Storyboard)this.Resources["Comer"];
            comer.Begin();
        }

        private void Jugar()
        {
            pbDiversion.Value += Dado(60);
            Storyboard malabares = (Storyboard)this.Resources["Malabares"];
            malabares.Begin();
        }

        private void Juego()
        {
            smash = new Smash(this);
            smash.Show();
            t1.Stop();
        }

        #endregion acciones

        private void reiniciarApp(object sender, RoutedEventArgs e)
        {
            Process.Start(Application.ResourceAssembly.Location);

            Application.Current.Shutdown();
        }

        public void anadirPuntos(int puntuacion) {
            pbDiversion.Value += puntuacion;
            avatar.Diversion += puntuacion;
        }
    }
}