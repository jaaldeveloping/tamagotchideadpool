# Deadpool #
Proyecto desarrollado para la asignatura de **INTERACIÓN PERSONA-ORDENADOR II** en la cual se planificó e implementó una aplicación de escritorio en **C#**, la cual era un tamagochi en el que el usuario del juego debía darle de comer, hacerle dormir o hacer que jugase. En paralelo se desarrolló el reconocimiento de voz para que el usuario pudiera controlar las actividades del tamagochi mediante el uso de la voz y frases. Se prestó una atención especial a los eventos emocionales del tamagochi desplegando una inteligencia emocional completa por cada acción que ejecutase el usuario.

![16.PNG](https://bitbucket.org/repo/RbML6K/images/2808381406-16.PNG)

![23.PNG](https://bitbucket.org/repo/RbML6K/images/2645270805-23.PNG)

![8.PNG](https://bitbucket.org/repo/RbML6K/images/1746218042-8.PNG)